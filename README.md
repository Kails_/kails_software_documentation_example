<!-- LOGO -->
<!-- ![](https://imgur.com/8rDeuAI) -->
<!-- <div align="center"> -->
<!--  <img width="600" src="https://imgur.com/8rDeuAI"> -->
<!-- </div> -->


## Index

* [Description](#Description)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Acknowledgements](#acknowledgements)


## Description
General description

### Main Goal
* Target 1, description
* Target 2, description

## Installation

### Requirements
* Requirement 1
* Requirement 2

1. do something
```sh
command
```
2. another thing
```sh
command 2
```

## Usage
```sh
command
```

### Usage examples
```sh
1) command args
2) aommand arg arg
```

### How to Config
1. things to do
2. another

## License
Distributed under the ```License Name``` license

## Acknowledgements

* Acknowledgment 1
* Acknowledgment 2
